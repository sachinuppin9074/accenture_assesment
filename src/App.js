import React from 'react';
import Second from './second.js';
import Third from './third.js';
import Fourth from './fourth.js';
import Fifth from './fifth.js';

class App extends React.Component{

    render(){

        return(

            <div>
                    <Second/>
                   <Third/>
                   <Fourth/>
                    <Fifth/>
            </div>
        );
      }

    }

export default App;