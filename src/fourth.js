import React from 'react';

class Fourth extends React.Component{

    render(){

            // Using split and reverse
            
            var output="";
        
            function rev_str(input) {
            
            var str = input.split(" ");
          
            for(let i=0;i<str.length;i++){
            
            var split_str = str[i].split("");
           
            var rev = split_str.reverse(); 
          
            var join_str = rev.join(""); 
          
            output = output + " " + join_str;
              
            }
              
            }
         
            rev_str("Hey how are you");
        
           
           
            //Without using Using split and reverse

            var myoutput = "";
                

            function reverse_str(input) {
  
                  
                var str = input.split(" ");
              
                for(let i=0;i<str.length;i++){
                
                   for(let j=str[i].length-1;j>=0;j--){
                          
                    myoutput = myoutput + "" +str[i][j];
                          
                    }
                    myoutput = myoutput + " ";
                }
                  
                console.log(myoutput)
            
            }
             
            reverse_str("Hey how are you");
            
        return(

            <div>
                <h3> Fourth :  </h3>

                    <h4> Using split and reverse </h4>
                    
                    { output }

                    <h4> Without Using split and reverse </h4>

                    { myoutput }
            </div>
            
        );
      }

    }

export default Fourth;