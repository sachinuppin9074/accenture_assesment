import React from 'react';

class Second extends React.Component{

    render(){

        var arr = ['hello world', 'hello world'];
    
        var map = new Map(); 

         for(var i=0; i<arr.length; i++){

        var c_arr = arr[i];

        for(let j=0;j<c_arr.length;j++){

        if(map.has(c_arr[j])){
            let value = map.get(c_arr[j])
            value++;
            map.set(c_arr[j],value);
        }
        else
        {
            map.set(c_arr[j],1);
         }

         }
        }

        var output = "";
        
        for (let c of map) {
            let [key, value] = c;
            output = output + key +":" + value + ",";
        }
   

        return(

            <div>
                <h3> Second :  </h3>

                { output }
              
            </div>
        );
      }

    }

export default Second;