import React from 'react';

class Third extends React.Component{

    render(){

        var input = {  a: 5, b: { c: { d: 10 } }};

        var output;

        function get_keys(input, a) {
  
            var obj =  Object.keys(input);
            obj.forEach(function(key) {
            a.push(key);
            a = get_keys(input[key], a);
      
        });
  
        return a;
        }

        output = get_keys(input, []);
        
        return(

            <div>
                <h3> Third :  </h3>

                { output }

              
            </div>
        );
      }

    }

export default Third;